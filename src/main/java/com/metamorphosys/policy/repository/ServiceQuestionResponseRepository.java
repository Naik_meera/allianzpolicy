package com.metamorphosys.policy.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.metamorphosys.policy.dataobjects.ServiceQuestionReponseDO;

public interface ServiceQuestionResponseRepository extends Repository<ServiceQuestionReponseDO, String>{

	//void save(List<ServiceQuestionReponseDO> serviceQuestionReponseDOList);
}
