package com.metamorphosys.policy.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.metamorphosys.policy.dataobjects.CarDetailsDO;

public interface CarDetailsRepository extends Repository<CarDetailsDO,String>{

	List<CarDetailsDO> findByVehicleNum(String vehiclenum);
}
