package com.metamorphosys.policy.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.policy.Executor.PolicyExecutor;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;
import com.metamorphosys.policy.utility.SerializationUtility;

@RestController
@RequestMapping("/api/policy")
final class PolicyController {

	private static final Logger LOGGER =  LoggerFactory.getLogger(PolicyController.class);
	
	private final PolicyExecutor policyExecutor;
	
	@Autowired
	PolicyController (PolicyExecutor policyExecutor) {
        this.policyExecutor = policyExecutor;
    }
	
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity init(){
		LOGGER.info("init PolicyController");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowMethods();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		CreatePolicyIO io = new CreatePolicyIO();
		PolicyDO policyDO= new PolicyDO();
		io.setPolicyDO(policyDO);
		io=policyExecutor.init(io);
		//LOGGER.info(SerializationUtility.getInstance().toJson(io));
		return new ResponseEntity(io, httpHeaders, httpStatus);		
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid CreatePolicyIO io,HttpServletRequest request) throws ClassNotFoundException, IOException{
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		//LOGGER.info("save PolicyController");
		io=policyExecutor.execute(io,request);
		
		return new ResponseEntity(io, httpHeaders, httpStatus);			
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ResponseEntity fetch(@PathVariable("id") Long id) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		//LOGGER.info("fetch PolicyController");
		
		PolicyDO policyDO = new PolicyDO();
		policyDO.setId(id);
		CreatePolicyIO io = new CreatePolicyIO();
		io.setPolicyDO(policyDO);
		io=policyExecutor.load(io);
		
		return new ResponseEntity(io, httpHeaders, httpStatus);	
		
	}
}

