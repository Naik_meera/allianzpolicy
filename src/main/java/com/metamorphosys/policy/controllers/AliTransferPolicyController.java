package com.metamorphosys.policy.controllers;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyActivityDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.master.CityRepository;
import com.metamorphosys.insureconnect.jpa.master.CountryRepository;
import com.metamorphosys.insureconnect.jpa.master.ProvinceRepository;
import com.metamorphosys.insureconnect.jpa.transaction.AgentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntityDocumentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyActivityRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PremiumSIMapRepository;
import com.metamorphosys.insureconnect.jpa.transaction.RetryServiceRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SequenceGeneratorUtil;
import com.metamorphosys.policy.Executor.PolicyExecutor;
import com.metamorphosys.policy.asyncservices.ThirdPartyUploads;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;
import com.metamorphosys.policy.repository.CarDetailsRepository;
import com.metamorphosys.policy.utility.SerializationUtility;

@RestController
@CrossOrigin
@RequestMapping("/api/ali/transferPolicy")
public class AliTransferPolicyController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AliTransferPolicyController.class);
	
	@Autowired
	ThirdPartyUploads thirdPartyUploads;
	
	@Autowired
	PolicyExecutor policyExecutor;

	@Autowired
	CarDetailsRepository carDetailsRepository;

	@Autowired
	SequenceGeneratorUtil sequenceGeneratorUtil;

	@Autowired
	ApplicationRepository applicationRepository;

	@Autowired
	PolicyActivityRepository policyActivityRepository;

	@Autowired
	RetryServiceRepository retryServiceRepository; //Change Made By Anna 30/01/17

	@Autowired
	AgentRepository agentRepository;

	@Autowired
	PolicyRepository policyRepository;
	
	@Autowired
	CityRepository cityRepo;

	@Autowired
	ProvinceRepository provinceRepo;

	@Autowired
	PremiumSIMapRepository premiumSIMapRepository;

	@Autowired
	CountryRepository countryRepo;
	
	@Autowired
	EntityDocumentRepository entityDocRep;


	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid String json,HttpServletRequest request) throws ClassNotFoundException, IOException {
		
		Date startTime=new Date();
		LOGGER.info("ALi Policy Received :"+startTime);
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date date = new Date() ;
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = (JsonObject) jsonParser.parse(json);
		String proposalReferenceId = jsonObject.get("proposalReferenceNum").getAsString();


		PolicyActivityDO policyActivityDO = policyActivityRepository.findByActivityKey(proposalReferenceId);
		if(policyActivityDO==null){
			policyActivityDO= new PolicyActivityDO();
			policyActivityDO.setActivityKey(proposalReferenceId);
			policyActivityDO.setActivityValue(json);
			policyActivityDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
			policyActivityDO.setActivityStatus(InsureConnectConstants.Status.PENDING);
		}else{
			policyActivityDO.setActivityValue(json);
			policyActivityDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
			policyActivityDO.setActivityStatus(InsureConnectConstants.Status.PENDING);
		}
		LOGGER.info("Saving activity");	
		policyActivityRepository.save(policyActivityDO);
		CreatePolicyIO io = (CreatePolicyIO) SerializationUtility.getInstance().fromJson(json, CreatePolicyIO.class);
		LOGGER.info("Policy Init");	
		io = policyExecutor.init(io);
		PolicyDO policyDO = io.getPolicyDO();
		int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
		PolicyDO policy=policyRepository.findByProposalRefNum(proposalReferenceId);
		if(policy==null)
		{
			try {
				if(timeFormat.parse(timeFormat.format(date)).after(timeFormat.parse("12:00:00")) && dateFormat.parse(dateFormat.format(policyDO.getPolicyCommencementDt())).equals(dateFormat.parse(dateFormat.format(date))))
				{
					String policyCommencementStr=dateFormat.format(policyDO.getPolicyCommencementDt().getTime()+MILLIS_IN_DAY);
					Date nextPolicyCommencementDt=dateFormat.parse(policyCommencementStr);
					policyDO.setPolicyCommencementDt(nextPolicyCommencementDt);

					String policyExpiryDtStr=dateFormat.format(policyDO.getPolicyExpiryDt().getTime()+MILLIS_IN_DAY);
					Date nextExpiryDt=dateFormat.parse(policyExpiryDtStr);
					policyDO.setPolicyExpiryDt(nextExpiryDt);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			LOGGER.info("Executing policy");
			io = policyExecutor.execute(io,request);
			String uwDecision = io.getPolicyDO().getUwDecisionCd();
			//String uwDecision = InsureConnectConstants.uwDecision.ACCEPTED;
			//			if(InsureConnectConstants.uwDecision.ACCEPTED.equals(io.getPolicyDO().getUwDecisionCd())){
			if(InsureConnectConstants.uwDecision.ACCEPTED.equals(uwDecision)){
				thirdPartyUploads.aliThirdPartyUpload(io);
				policyActivityDO.setActivityStatus(InsureConnectConstants.Status.SUCCESS);
				policyActivityRepository.save(policyActivityDO);
			}
		}else{
			io.getPolicyDO().setPolicyNum(policy.getPolicyNum());
		}
		return new ResponseEntity(io, httpHeaders, httpStatus);
	}
	
}

