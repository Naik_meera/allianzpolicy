package com.metamorphosys.policy.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api/ping")
public class PingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PingController.class);

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity ping(@RequestBody @Valid String json) {
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Input String-->"+json);
		LOGGER.info("ping PingController");

		// Call PolicyCOnverter for demo POC

		return new ResponseEntity("SUCCESS", httpHeaders, httpStatus);
	}

}

