package com.metamorphosys.policy;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages={"com.metamorphosys"})
public class PolicyApplication {

	public static void main(String[] args) {
		ApplicationContext ctx=(ApplicationContext) SpringApplication.run(PolicyApplication.class, args);
		
	}
}
