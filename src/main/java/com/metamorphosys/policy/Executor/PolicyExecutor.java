package com.metamorphosys.policy.Executor;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.utilities.SequenceGeneratorUtil;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;
import com.metamorphosys.policy.utility.GuidGeneratorUtility;
import com.metamorphosys.policy.utility.PolicyConversionUtil;
import com.metamorphosys.policy.utility.SerializationUtility;

@Component
public class PolicyExecutor extends AbstractExecutor{

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyExecutor.class);

	@Autowired
	SequenceGeneratorUtil sequenceGeneratorUtil;
	
	@Autowired
	PolicyConversionUtil policyConversion;
	
	@Override
	public CreatePolicyIO init(CreatePolicyIO io) {
		if(io.getServiceDO()==null){
			ServiceDO serviceDO = getService();
			serviceDO.setServiceDt(io.getPolicyDO().getRuleEffectiveDate());
			io.setServiceDO(serviceDO);
//			io.setServiceDO(getService());
		}
		super.init(io);
		return io;
	}
	
	@Override
	public CreatePolicyIO execute(CreatePolicyIO io,HttpServletRequest request) throws ClassNotFoundException, IOException{
		PolicyDO policyDO = (PolicyDO) io.getPolicyDO();
		getCustomerId(policyDO.getPolicyClientList());
		getPolicyNum(policyDO);
		
		super.execute(io,request);
		
		return io;
	}

	@Override
	public void postExecute(CreatePolicyIO io,HttpServletRequest request) {		
		
		//Saving policy and inbox in Demo Project . commented by swapnil on 2 feb 2017.
//		policyConversion.convertionUtil(io);
		
		//Call Case for copying Demo case
		PolicyDO policyDO= (PolicyDO) io.getPolicyDO() ;
		LinkedTreeMap map = (LinkedTreeMap) io.getResultDO().getResultMap().get("CaseDO");
		if(map!=null){
			//JsonElement element = (JsonElement) SerializationUtility.getInstance().fromJson(map.toString(),JsonElement.class );
			//JsonObject object =element.getAsJsonObject();
			map.put("uwDecisionCd", io.getPolicyDO().getUwDecisionCd());
			
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders httpHeaders = new HttpHeaders();
			//httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				httpHeaders.set(key,  request.getHeader(key));
			}
			HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(map), httpHeaders);
//			ResponseEntity<String> jsonObject = restTemplate.exchange("http://localhost:8090/api/copyCase",HttpMethod.POST, entity, String.class);
		}
		
		//Saving policy and inbox in Demo Project . commented by swapnil on 2 feb 2017
//		policyConversion.convertionDoc(io);
		
		super.postExecute(io,request);
	}

	private void getPolicyNum(PolicyDO policyDO) {
	//This to be done for allianz requirement by bhavini 02/01/2017
		//if (policyDO.getPolicyNum() == null) {
		if (policyDO.getProposalNum() == null) {
			policyDO.setPolicyNum(sequenceGeneratorUtil.generateNextSeq("policyNum"));
			policyDO.setProposalNum(policyDO.getPolicyNum());
		}
	}


	private void getCustomerId(List<PolicyClientDO> policyClientList) {
		for (PolicyClientDO policyClientDO : policyClientList) {
			if (policyClientDO.getCustomerId() == null) {
				policyClientDO.setCustomerId(sequenceGeneratorUtil.generateNextSeq("clientId").toString());
			}
		}

	}

	@Override
	public ServiceDO getService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("CREATEPROPOSAL");
		serviceDO.setServiceType("Workflow");
		Date d= new Date(); 
		java.sql.Date dt = new java.sql.Date(d.getTime()); 
 		serviceDO.setServiceDt(dt);
		serviceDO.setGuid(GuidGeneratorUtility.getInstance().generateGuid());
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("init");
		serviceDO.getServiceTaskList().add(task);
		return serviceDO;
	}

	}
