package com.metamorphosys.policy.Executor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntitySignatureDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.jpa.transaction.EntityDocumentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntitySignatureRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyRepository;
import com.metamorphosys.insureconnect.utilities.CommonUtil;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.policy.dataobjects.common.ResultDO;
import com.metamorphosys.policy.interfaceobjects.BaseIO;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;
import com.metamorphosys.policy.repository.ServiceQuestionResponseRepository;
import com.metamorphosys.policy.utility.SerializationUtility;

public abstract class AbstractExecutor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExecutor.class);
	
	@Autowired
	PolicyRepository policyRepository;
	
	@Autowired
	ServiceQuestionResponseRepository serviceQuestionResponseRepository;
	
	@Autowired
	EntityDocumentRepository entityDocumentRepository;
	
	@Autowired
	EntitySignatureRepository entitySignatureRepository;
	
	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired
	WebGetController webGetController;
	
	public CreatePolicyIO init(CreatePolicyIO io) {
		io = callRuleEngine(io,InsureConnectConstants.Method.EXECUTE);
		LinkedTreeMap map= (LinkedTreeMap) io.getResultDO().getResultMap().get(io.getServiceDO().getService());
		io.getPolicyDO().setGuid((String) map.get("guid"));
		return io;
		
	}

	public CreatePolicyIO execute(CreatePolicyIO io,HttpServletRequest request) throws ClassNotFoundException, IOException {
		//io=convertToBaseIO(io);
		io = callRuleEngine(io,InsureConnectConstants.Method.EXECUTE);
		
		String val= (String) io.getResultDO().getResultMap().get(InsureConnectConstants.UWDECISIONCD);
		if(val!=null){
			io.getPolicyDO().setUwDecisionCd(val);
		}
		LOGGER.info("Saving Policy data");
		//Added condition for allianz by bhavini 
//		if(InsureConnectConstants.uwDecision.ACCEPTED.equals(io.getPolicyDO().getUwDecisionCd())){
			if(io.getEntityDocumentDOs()!=null && io.getEntityDocumentDOs().size()>0){
				Set<EntityDocumentDO> entityDocSet = new HashSet<EntityDocumentDO>(io.getEntityDocumentDOs());
				entityDocumentRepository.save(entityDocSet);
			}
			if(io.getEntitySignatureDOs()!=null && io.getEntitySignatureDOs().size()>0){
				Set<EntitySignatureDO> entitySignSet = new HashSet<EntitySignatureDO>(io.getEntitySignatureDOs());
				entitySignatureRepository.save(entitySignSet);
			}
			
			policyRepository.save((PolicyDO) io.getPolicyDO());
			LOGGER.info("Policy data saved");
			postExecute(io, request);
//		}
		return io;
	}

	public CreatePolicyIO load(CreatePolicyIO io) {
		
		io = callRuleEngine(io,InsureConnectConstants.Method.GET);
		return io;
	}
	
	private CreatePolicyIO callRuleEngine(CreatePolicyIO io,String method) {
		LOGGER.info("Calling Rule Engine");
		String tojson = covertTojson(io);
		//toJson is BasIO string
		//LOGGER.info(tojson);
		ResponseEntity<String> jsonObject=null;
		if(InsureConnectConstants.Method.EXECUTE.equals(method)){
			jsonObject = webExecuteController.executeJson(tojson);
		}else if(InsureConnectConstants.Method.GET.equals(method)){
			jsonObject = webGetController.execute(tojson);
		}
		/*RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(tojson.toString(), httpHeaders);
		ResponseEntity<String> jsonObject = restTemplate.exchange(url,
				HttpMethod.POST, entity, String.class);*/
		io = convertjsonToObject(jsonObject.getBody(), io);
		LOGGER.info("Rule Engine processed");
		return io;
	}

	private CreatePolicyIO convertjsonToObject(String json, CreatePolicyIO io) {

		String data = null;
		LinkedTreeMap map = SerializationUtility.getInstance().fromJson(json);

		if (null != map && map.containsKey("data")) {
			data = map.get("data").toString();
		}
		
		ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) SerializationUtility.getInstance().fromJson(data, ArrayList.class);
		if (null != responseDataList) {
			for (LinkedTreeMap responseDataIO : responseDataList) {
				if (InsureConnectConstants.DataObjects.POLICYDO.equals(responseDataIO.get("objectName").toString())) {

					PolicyDO baseDO = (PolicyDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), PolicyDO.class);
					io.setPolicyDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.SERVICEDO.equals(responseDataIO.get("objectName").toString())) {
					ServiceDO baseDO = (ServiceDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ServiceDO.class);

					io.setServiceDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.RESULTDO.equals(responseDataIO.get("objectName").toString())) {
					ResultDO baseDO = (ResultDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ResultDO.class);

					io.setResultDO(baseDO);
					setOvverideServiceData(io);
				}

			}
		}
		return io;
	}

	private void setOvverideServiceData(CreatePolicyIO io) {
		if(io.getServiceDO().getServiceTaskList().size()>0 && io.getResultDO()!=null){
			LinkedTreeMap map = (LinkedTreeMap) io.getResultDO().getResultMap().get("WorkflowServiceDO");
			if(map!=null && map.size()>0){
				ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) map.get("workflowServiceTaskList");
				for (LinkedTreeMap responseDataIO : responseDataList) {
					if(InsureConnectConstants.Status.PENDING.equals(responseDataIO.get(InsureConnectConstants.STATUS))){
						io.getServiceDO().getServiceTaskList().get(0).setServiceTask((String) responseDataIO.get("serviceTask"));
					}
				}
			}
			//
		}
		
	}

	public String covertTojson(CreatePolicyIO io) {
		List<BaseIO> baseIOs = new ArrayList<BaseIO>();

		BaseIO baseIO = new BaseIO();
		baseIO.setObjectName(io.getPolicyDO().getObjectName());
		baseIO.setObject(SerializationUtility.getInstance().toJson(io.getPolicyDO()));
		baseIOs.add(baseIO);

		if(io.getServiceDO()!=null){
			BaseIO baseIO1 = new BaseIO();
			baseIO1.setObjectName(io.getServiceDO().getObjectName());
			baseIO1.setObject(SerializationUtility.getInstance().toJson(io.getServiceDO()));
			baseIOs.add(baseIO1);
		}

		String serialize = SerializationUtility.getInstance().toJson(baseIOs);
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("data", serialize);
		String finalString = SerializationUtility.getInstance().toJson(dataMap);
		

		return finalString;

	}

	public abstract ServiceDO getService();
	
	public CreatePolicyIO convertToBaseIO(CreatePolicyIO io) throws ClassNotFoundException, IOException{
		
		PolicyCoverageDO coverageDO = io.getPolicyDO().getBasePlan();
		List<PolicyCoverageDO> coverageDO1 = new ArrayList<PolicyCoverageDO>();
		
		for(PolicyCoverageDO coverageDO2 : io.getPolicyDO().getPolicyCoverageList()){
			List<PolicyCoverageDO> coverageDOs = new ArrayList<PolicyCoverageDO>();
			coverageDOs.add(coverageDO);
			PolicyCoverageDO coverageDO3=(PolicyCoverageDO) CommonUtil.getInstance().cloneObject(coverageDO2);
			coverageDO3.setPlanTypeCd(null);
			if(coverageDO3.getPolicyCoverageInsuredList().size()>0){
				coverageDO3.getPolicyCoverageInsuredList().get(0).setRoleCd(null);
			}
			coverageDOs.add(coverageDO3);
			io.getPolicyDO().setPolicyCoverageList(coverageDOs);
			BaseIO baseIO = new BaseIO();
			baseIO.setObject(SerializationUtility.getInstance().toJson(io.getPolicyDO()));
			baseIO.setObjectName(io.getPolicyDO().getObjectName());
			
	/*		MultiValueMap parameters = new LinkedMultiValueMap();
			parameters.add("baseIO", baseIO);*/
			//parameters.add("productCd", io.getPolicyDO().getBaseProductCd());

			
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(baseIO), httpHeaders);
			ResponseEntity<String> jsonObject = restTemplate.exchange("http://localhost:8079/api/validateProduct/"+coverageDO2.getProductCd(),HttpMethod.POST, entity, String.class);
			
			CreatePolicyIO io2 = convertjsonToObject(jsonObject.getBody(), io);
			coverageDO1.add(coverageDO2);
			if(io.getResultDO()==null){
				if(io2.getResultDO().getResultMap()!=null && io.getResultDO().getResultMap()==null){
					io.getResultDO().setResultMap(io2.getResultDO().getResultMap());
				}else if(io2.getResultDO().getResultMap()!=null && io.getResultDO().getResultMap()!=null){
					io.getResultDO().getResultMap().putAll(io2.getResultDO().getResultMap());
				}
				
				if(io2.getResultDO().getExceptionList()!=null && io.getResultDO().getExceptionList()==null){
					io.getResultDO().setExceptionList(io2.getResultDO().getExceptionList());
				}else if(io2.getResultDO().getExceptionList()!=null && io.getResultDO().getExceptionList()!=null){
					io.getResultDO().getExceptionList().addAll(io2.getResultDO().getExceptionList());
				}
			}else{
				io.setResultDO(io2.getResultDO());
			}
			
			io.getPolicyDO().setPolicyCoverageList(coverageDO1);
			
		}
		return io;
	}

	public void postExecute(CreatePolicyIO io, HttpServletRequest request) {
		// TODO Auto-generated method stub
		
	}

}
