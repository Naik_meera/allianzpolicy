package com.metamorphosys.policy.Executor;

import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;

public interface PolicyService {

	public CreatePolicyIO execute(CreatePolicyIO io);

	public CreatePolicyIO init(CreatePolicyIO io);
	
}
