package com.metamorphosys.policy.interfaceobjects;


import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntitySignatureDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.policy.dataobjects.ServiceQuestionReponseDO;
import com.metamorphosys.policy.dataobjects.common.ResultDO;

public class CreatePolicyIO {

	public PolicyDO policyDO;
	
	public List<PolicyDO> policyDOList;//Not Used in motor

	public ServiceDO serviceDO;
	
	public List<ServiceQuestionReponseDO> serviceQuestionReponseDOs;//Not Used in motor
	
	public List<EntityDocumentDO> entityDocumentDOs;
	
	public List<EntitySignatureDO> entitySignatureDOs;
	
	public ResultDO resultDO;

	public List<PolicyDO> getPolicyDOList() {
		return policyDOList;
	}

	public void setPolicyDOList(List<PolicyDO> policyDOList) {
		this.policyDOList = policyDOList;
	}

	public ResultDO getResultDO() {
		return resultDO;
	}

	public void setResultDO(ResultDO resultDO) {
		this.resultDO = resultDO;
	}

	public List<ServiceQuestionReponseDO> getServiceQuestionReponseDOs() {
		return serviceQuestionReponseDOs;
	}

	public void setServiceQuestionReponseDOs(List<ServiceQuestionReponseDO> serviceQuestionReponseDOs) {
		this.serviceQuestionReponseDOs = serviceQuestionReponseDOs;
	}

	public List<EntityDocumentDO> getEntityDocumentDOs() {
		return entityDocumentDOs;
	}

	public void setEntityDocumentDOs(List<EntityDocumentDO> entityDocumentDOs) {
		this.entityDocumentDOs = entityDocumentDOs;
	}

	public PolicyDO getPolicyDO() {
		return policyDO;
	}

	public void setPolicyDO(PolicyDO policyDO) {
		this.policyDO = policyDO;
	}

	public ServiceDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(ServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}

	public List<EntitySignatureDO> getEntitySignatureDOs() {
		return entitySignatureDOs;
	}

	public void setEntitySignatureDOs(List<EntitySignatureDO> entitySignatureDOs) {
		this.entitySignatureDOs = entitySignatureDOs;
	}
	
}
