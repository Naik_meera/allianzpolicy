package com.metamorphosys.policy.utility;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.HashMapDataDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.HashMapEntityDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientAddressDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientContactDetailsDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageInsuredDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.jpa.transaction.HashMapEntityRepository;
import com.metamorphosys.insureconnect.utilities.DateUtil;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;

@Component
public class PolicyConversionUtil {

	private static PolicyConversionUtil utility;

	/*public static PolicyConversionUtil getInstance() {
		if (null == utility) {
			utility = new PolicyConversionUtil();
		}

		return utility;
	}*/

	@Autowired
	HashMapEntityRepository hashMapEntityRepository;

	public void convertionUtil(CreatePolicyIO createPolicyIO) {
		PolicyDO policyDO = (PolicyDO) createPolicyIO.getPolicyDO();
		Connection c = null;

		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mydb", "postgres", "root");
			c.setAutoCommit(false);

			if ("MOTOR".equals(policyDO.getPolicyTypeCd())) {
				convertionForMotor(createPolicyIO, c);
			} else if ("HEALTH".equals(policyDO.getPolicyTypeCd())) {
				convertionForHealth(createPolicyIO, c);
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	private void convertionForHealth(CreatePolicyIO createPolicyIO, Connection c) {
		try {
			PolicyDO policyDO = (PolicyDO) createPolicyIO.getPolicyDO();
			PreparedStatement preparedStatement = null;
			String policyQuery = "INSERT INTO health_policy_master (\"Policy_Type\", h_policy_no, \"Plan_Type\",\"Family_Floater_Details1\""
					+ ",\"Addon_covers_Opted1\",sum_insured,proposed_policy,sdt,status,username,user_type,\"Product\",\"Premium\""
					+ ") VALUES (? ,? ,?, ?, ?, ? ,? ,? ,? ,? ,? ,?, ?);";

			preparedStatement = c.prepareStatement(policyQuery);
			preparedStatement.setString(1, "HEALTH");
			preparedStatement.setString(2, policyDO.getPolicyNum());
			preparedStatement.setString(3, "BASEPLAN");
			preparedStatement.setString(4, null);
			preparedStatement.setString(5, null);
			if(policyDO.getFinalSumInsured()!=null){
				preparedStatement.setString(6, policyDO.getFinalSumInsured().toString());
			}else{
				preparedStatement.setString(6,null);
			}
			preparedStatement.setString(7,null);
			preparedStatement.setString(8, null);
			preparedStatement.setString(9, "ACTIVE");
			preparedStatement.setString(10, "Health");
			preparedStatement.setString(11, "HealthAdmin");
			preparedStatement.setString(12, policyDO.getBaseProductCd());
			if(policyDO.getBasePlan().getBaseAnnualPremium()!=null){
				preparedStatement.setString(13, policyDO.getBasePlan().getBaseAnnualPremium().toString());
			}else{
				preparedStatement.setString(13, null);
			}

			preparedStatement.execute();
			for (PolicyClientDO clientDO : policyDO.getPolicyClientList()) {
				PreparedStatement preparedStatement2 = null;
				if ("PROPOSER".equals(clientDO.getRoleCd())) {
					String proposerDetails = "Insert into public.health_proposer_details(fname ,mname ,lname ,address,city ,"
							+ "state ,pincode,mobile,landmark ,road ,country ,email ,id_proof ,aIncome,id_proof_detail ,marital_status ,profession "
							+ "nationality ,profession_details ,acc_no ,h_policy_no  ) VALUES "
							+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

					preparedStatement2 = c.prepareStatement(proposerDetails);
					preparedStatement2.setString(1, clientDO.getFirstName());
					preparedStatement2.setString(2, clientDO.getMiddleName());
					preparedStatement2.setString(3, clientDO.getLastName());
					for (PolicyClientAddressDO addressDO : clientDO.getPolicyClientAddressList()) {
						if ("PERMANENT".equals(addressDO.getAddressTypeCd())) {
							preparedStatement2.setString(4, addressDO.getAddressLine1()+" "+addressDO.getAddressLine2());
							preparedStatement2.setString(5, addressDO.getCityCd());
							preparedStatement2.setString(6, addressDO.getStateCd());
							preparedStatement2.setString(7, addressDO.getPostCd().toString());
							preparedStatement2.setString(8, addressDO.getAddressLine3());
							preparedStatement2.setString(9, addressDO.getAddressLine4());
							preparedStatement2.setString(10, addressDO.getCountryCd());
						}
					}
					for(PolicyClientContactDetailsDO policyClientContactDetailsDO:clientDO.getPolicyClientContactDetailsList()){
						if("EMAIL".equals(policyClientContactDetailsDO.getContactTypeCd())){
							preparedStatement2.setString(11, policyClientContactDetailsDO.getEmailId());
							break;
						}
					}
					preparedStatement2.setString(12, clientDO.getIdentityTypeCD());
					preparedStatement2.setString(13, clientDO.getMontlyIncome().toString());
					preparedStatement2.setString(14, clientDO.getIdentityNum());
					preparedStatement2.setString(15, clientDO.getMaritalStatusCd());
					preparedStatement2.setString(16, clientDO.getOccupationCd());
					preparedStatement2.setString(17, "INDIAN");
					preparedStatement2.setString(18, clientDO.getNatureOfDutyCd());
					preparedStatement2.setString(19, null);
					preparedStatement2.setString(20, policyDO.getPolicyNum());
					preparedStatement2.execute();
				} else if ("NOMINEE".equals(clientDO.getRoleCd())) {
					String nomineeDetails = "Insert into public.health_nomination(fnm ,mnm ,lnm ,rela ,address ,h_policy_no ) VALUES"
							+ " (?,?,?,?,?,?)";

					preparedStatement2 = c.prepareStatement(nomineeDetails);
					preparedStatement2.setString(1, clientDO.getFirstName());
					preparedStatement2.setString(2, clientDO.getMiddleName());
					preparedStatement2.setString(3, clientDO.getLastName());
					preparedStatement2.setString(4, clientDO.getRelationCd());
					for (PolicyClientAddressDO addressDO : clientDO.getPolicyClientAddressList()) {
						if ("PERMANENT".equals(addressDO.getAddressTypeCd())) {
							preparedStatement2.setString(5, addressDO.getAddressLine1()+" "+addressDO.getAddressLine2());
						}
					}
					preparedStatement2.setString(6, policyDO.getPolicyNum());
					preparedStatement2.execute();
				} else if ("PRIMARY".equals(clientDO.getRoleCd())) {
					String primaryDetails = "Insert into public.health_nomination(fnm ,mnm ,lnm ,gen  ,height ,"
							+ "wt ,dob,rel,edu,occu ,h_policy_no ) VALUES" + " (?,?,?,?,?,?,?,?,?,?,?)";

					preparedStatement2 = c.prepareStatement(primaryDetails);
					preparedStatement2.setString(1, clientDO.getFirstName());
					preparedStatement2.setString(2, clientDO.getMiddleName());
					preparedStatement2.setString(3, clientDO.getLastName());
					preparedStatement2.setString(4, clientDO.getGenderCd());
					preparedStatement2.setString(5, null);
					preparedStatement2.setString(6, null);
					preparedStatement2.setString(7, clientDO.getDateOfBirth().toString());
					preparedStatement2.setString(8, clientDO.getRelationCd());
					preparedStatement2.setString(9, null);
					preparedStatement2.setString(10, clientDO.getOccupationCd());
					preparedStatement2.setString(11, policyDO.getPolicyNum());
					preparedStatement2.execute();
				}
			}
			for (PolicyCoverageDO coverageDO : policyDO.getPolicyCoverageList()) {
				if (!"BASEPLAN".equals(coverageDO.getPlanTypeCd())) {
					PreparedStatement preparedStatement4 = null;
					String riderDetails = "INSERT INTO public.Riders(policy_no, Policy_Type, Plan_Type, sum_insured,proposed_policy,"
							+ "sdt, status, username, "
							// + "vehmanufactureyear,vehregistrationdate,
							+ "user_type,Product)" + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

					preparedStatement4 = c.prepareStatement(riderDetails);
					preparedStatement4.setInt(1, Integer.parseInt(policyDO.getPolicyNum()));
					preparedStatement4.setString(2, "HEALTH");
					preparedStatement4.setString(3, coverageDO.getPlanTypeCd());
					preparedStatement4.setString(4, coverageDO.getSumInsured().toString());
					preparedStatement4.setString(5, null);
					preparedStatement4.setString(6, null);
					preparedStatement4.setString(7, "ACTIVE");
					preparedStatement4.setString(8, "Health");
					preparedStatement4.setString(9, "HealthAdmin");
					preparedStatement4.setString(10, coverageDO.getProductCd());

					preparedStatement4.execute();
				}
			}
			PreparedStatement preparedStatement5 = null;
			String inboxQuery = "INSERT INTO public.task_list(ref_no, task, task_date, task_type, id, user_type, username)VALUES (?, ?, ?, ?, ?, ?, ?);";
			preparedStatement5 = c.prepareStatement(inboxQuery);
			preparedStatement5.setString(1, policyDO.getPolicyNum());
			preparedStatement5.setString(2, createPolicyIO.getServiceDO().getService());

			java.util.Date d = new java.util.Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String date = dateFormat.format(d);

			preparedStatement5.setString(3, date);
			preparedStatement5.setString(4, "STP");
			preparedStatement5.setInt(5, Integer.parseInt(policyDO.getPolicyNum()));
			preparedStatement5.setString(6, "HealthAdmin");
			preparedStatement5.setString(7, "Health");
			if (createPolicyIO.getServiceDO().getServiceTaskList().size() > 0) {

			} else {
				preparedStatement5.setString(4, null);
			}
			preparedStatement5.execute();

			c.commit();
			c.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void convertionForMotor(CreatePolicyIO createPolicyIO, Connection c) {
		try {
			PreparedStatement preparedStatement = null;
			PolicyDO policyDO = (PolicyDO) createPolicyIO.getPolicyDO();

			String policyQuery = "INSERT INTO policy_master (app_date, policy_no, product,policy_master_id) VALUES (?, ?, ?, ?);";

			preparedStatement = c.prepareStatement(policyQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setDate(1, (Date) policyDO.getProposalReceivedDt());
			preparedStatement.setString(2, policyDO.getPolicyNum());
			preparedStatement.setString(3, policyDO.getBaseProductCd());
			preparedStatement.setInt(4, Integer.parseInt(policyDO.getPolicyNum()));
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			int generatedPolicyKey = 0;
			if (rs.next()) {
				generatedPolicyKey = rs.getInt(1);
			}

			preparedStatement.close();

			for (PolicyClientDO clientDO : policyDO.getPolicyClientList()) {
				PreparedStatement preparedStatement2 = null;
				if ("PROPOSER".equals(clientDO.getRoleCd())) {
					String email = null;
					String mobContact = null, resiContact = null, offContact = null;

					String policyHolder = "INSERT INTO policy_holder_details(phid,firstnm, lastnm,middlenm,pemail,pgender,pmobile,pmstatus,poccupation,  ptelephoneofc ,  ptelephoneres )"
							+ "  VALUES (?, ?, ?, ?, ?, ?, ?,? ,? , ? , ?);";

					preparedStatement2 = c.prepareStatement(policyHolder, Statement.RETURN_GENERATED_KEYS);
					// preparedStatement2.setInt(1,
					// Integer.parseInt(clientDO.getClientID()));
					preparedStatement2.setInt(1, Integer.parseInt(policyDO.getPolicyNum()));
					preparedStatement2.setString(2, clientDO.getFirstName());
					preparedStatement2.setString(3, clientDO.getLastName());
					if (clientDO.getMiddleName() != null) {
						preparedStatement2.setString(3, clientDO.getMiddleName());
					} else {
						preparedStatement2.setString(3, null);
					}
					// preparedStatement2.setDate(4, (Date)
					// clientDO.getDateOfBirth());
					for (PolicyClientContactDetailsDO clientContactDetailsDO : clientDO
							.getPolicyClientContactDetailsList()) {
						if ("EMAIL".equals(clientContactDetailsDO.getContactTypeCd())) {
							email = clientContactDetailsDO.getEmailId();
						}
						if ("MOBILE".equals(clientContactDetailsDO.getContactTypeCd())) {
							mobContact = clientContactDetailsDO.getContactNum();

						}
						if ("OFFICE".equals(clientContactDetailsDO.getContactTypeCd())) {
							offContact = clientContactDetailsDO.getContactNum();
						}
						if ("RESIDENCE".equals(clientContactDetailsDO.getContactTypeCd())) {
							resiContact = clientContactDetailsDO.getContactNum();
						}
					}
					if (email != null) {
						preparedStatement2.setString(4, email);
					} else {
						preparedStatement2.setString(4, null);
					}
					preparedStatement2.setString(5, clientDO.getGenderCd());
					if (clientDO.getIncome() != null) {
						preparedStatement2.setDouble(6, clientDO.getIncome());
					} else {
						preparedStatement2.setDouble(6, 0);
					}

					if (mobContact != null) {
						preparedStatement2.setString(7, mobContact);
					} else {
						preparedStatement2.setString(7, "0");
					}

					preparedStatement2.setString(8, clientDO.getMaritalStatusCd());

					preparedStatement2.setString(9, clientDO.getOccupationCd());

					if (offContact != null) {
						preparedStatement2.setString(10, offContact);
					} else {
						preparedStatement2.setString(10, "0");
					}

					if (resiContact != null) {
						preparedStatement2.setString(11, resiContact);
					} else {
						preparedStatement2.setString(11, "0");
					}
					//
					// preparedStatement2.setInt(14, generatedPolicyKey);

					preparedStatement2.execute();

					ResultSet rs1 = preparedStatement2.getGeneratedKeys();
					int generatedPhKey = 0;
					if (rs1.next()) {
						generatedPhKey = rs1.getInt(1);
					}

					for (PolicyClientAddressDO addressDO : clientDO.getPolicyClientAddressList()) {
						PreparedStatement preparedStatement3 = null;
						if ("PERMANENT".equals(addressDO.getAddressTypeCd())) {
							String permanaentAddress = "INSERT INTO ph_permanentadd(pcity,phouse,plandmark,ppincode,proad,pstate,pcountry,paddid"
									+ ")VALUES ( ?,?,?,?,?,?,?,?);";
							preparedStatement3 = c.prepareStatement(permanaentAddress);
							preparedStatement3.setString(1, addressDO.getCityCd());
							preparedStatement3.setString(2, addressDO.getAddressLine1());
							if (addressDO.getAddressLine2() != null) {
								preparedStatement3.setString(3, addressDO.getAddressLine2());
							} else {
								preparedStatement3.setString(3, null);
							}
							if (addressDO.getPostCd() != null) {
								preparedStatement3.setLong(4, addressDO.getPostCd());
							} else {
								preparedStatement3.setString(4, null);
							}
							if (addressDO.getAddressLine3() != null) {
								preparedStatement3.setString(5, addressDO.getAddressLine3());
							} else {
								preparedStatement3.setString(5, null);
							}
							preparedStatement3.setString(6, addressDO.getStateCd());
							preparedStatement3.setString(7, addressDO.getCountryCd());
							preparedStatement3.setInt(8, Integer.parseInt(policyDO.getPolicyNum()));
							// preparedStatement3.setInt(9, generatedPhKey);

							preparedStatement3.execute();

						} else {
							String corrAddress = "INSERT INTO ph_correspondenceadd(caddid,ccity,chouse,clandmark,cpin,croad,cstate,ccountry)"
									+ "VALUES (?,?,?,?,?,?,?,?);";
							preparedStatement3 = c.prepareStatement(corrAddress);
							preparedStatement3.setInt(1, Integer.parseInt(policyDO.getPolicyNum()));
							preparedStatement3.setString(2, addressDO.getCityCd());
							preparedStatement3.setString(3, addressDO.getAddressLine1());
							if (addressDO.getAddressLine2() != null) {
								preparedStatement3.setString(4, addressDO.getAddressLine2());
							} else {
								preparedStatement3.setString(4, null);
							}
							if(addressDO.getPostCd()!=null){
								preparedStatement3.setLong(5, addressDO.getPostCd());
							}else{
								preparedStatement3.setLong(5, 0);
							}
							if (addressDO.getAddressLine3() != null) {
								preparedStatement3.setString(6, addressDO.getAddressLine3());
							} else {
								preparedStatement3.setString(6, null);
							}
							preparedStatement3.setString(7, addressDO.getStateCd());
							// preparedStatement3.setInt(9, generatedPhKey);
							preparedStatement3.setString(8, addressDO.getCountryCd());

							preparedStatement3.execute();
						}
					}
				} else if ("NOMINEE".equals(clientDO.getRoleCd())) {
					String nominee = "INSERT INTO public.motor_nomination(fnm, lnm,mnm,gen,hom,land,rd,cty,stat,cntry,pinc,h_policy_no)"
							+ "VALUES (?, ?,?,?,?,?,?,?,?,?,?,?);";

					preparedStatement2 = c.prepareStatement(nominee);
					// preparedStatement2.setString(1, clientDO.getTitleCd());
					preparedStatement2.setString(1, clientDO.getFirstName());
					preparedStatement2.setString(2, clientDO.getLastName());
					if (clientDO.getMiddleName() != null) {
						preparedStatement2.setString(3, clientDO.getMiddleName());
					} else {
						preparedStatement2.setString(3, null);
					}
					// preparedStatement2.setDate(5, (Date)
					// clientDO.getDateOfBirth());
					preparedStatement2.setString(4, clientDO.getGenderCd());
					if (clientDO.getPolicyClientAddressList() != null
							&& clientDO.getPolicyClientAddressList().size() > 0) {
						for (PolicyClientAddressDO addressDO : clientDO.getPolicyClientAddressList()) {
							preparedStatement2.setString(5, addressDO.getAddressLine1());
							if (addressDO.getAddressLine2() != null) {
								preparedStatement2.setString(6, addressDO.getAddressLine2());
							} else {
								preparedStatement2.setString(6, null);
							}
							if (addressDO.getAddressLine3() != null) {
								preparedStatement2.setString(7, addressDO.getAddressLine3());
							} else {
								preparedStatement2.setString(7, null);
							}
							preparedStatement2.setString(8, addressDO.getCityCd());
							preparedStatement2.setString(9, addressDO.getStateCd());
							preparedStatement2.setString(10, addressDO.getCountryCd());
							preparedStatement2.setLong(11, addressDO.getPostCd());
						}
					} else {
						preparedStatement2.setString(5, null);
						preparedStatement2.setString(6, null);
						preparedStatement2.setString(7, null);
						preparedStatement2.setString(8, null);
						preparedStatement2.setString(9, null);
						preparedStatement2.setString(10, null);
						preparedStatement2.setLong(11, 0);
					}
					preparedStatement2.setString(12, policyDO.getPolicyNum());

					preparedStatement2.execute();
				}
			}

			for (PolicyCoverageDO coverageDO : policyDO.getPolicyCoverageList()) {
				if ("BASEPLAN".equals(coverageDO.getPlanTypeCd())) {
					for (PolicyCoverageInsuredDO coverageInsuredDO : coverageDO.getPolicyCoverageInsuredList()) {
						PreparedStatement preparedStatement4 = null;
						String vehicleDetails = "INSERT INTO public.vehicle_details(vehid, vehchassisnostring, vehcolor, vehcubiccapacity,vehengineno,"
								+ "vehfueltype, vehmake, vehmodel, "
								// + "vehmanufactureyear,vehregistrationdate,
								+ "vehregistrationno,policy_master_id,vehbodytype)"
								+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
						/*
						 * String vehicleDetails=
						 * "INSERT INTO public.vehicle_details(vehid, vehchassisnostring)"
						 * +"VALUES(?,?)";
						 */

						preparedStatement4 = c.prepareStatement(vehicleDetails);
						preparedStatement4.setInt(1, Integer.parseInt(policyDO.getPolicyNum()));
						preparedStatement4.setString(2, coverageInsuredDO.getChasisNum());
						preparedStatement4.setString(3, coverageInsuredDO.getVehicleColour());
						if (coverageInsuredDO.getEngineCapacity() != null) {
							preparedStatement4.setString(4, coverageInsuredDO.getEngineCapacity());
						} else {
							preparedStatement4.setString(4, null);
						}
						if (coverageInsuredDO.getEngineNum() != null) {
							preparedStatement4.setString(5, coverageInsuredDO.getEngineNum());
							// setLong(5, coverageInsuredDO.getEngineNum());
						} else {
							preparedStatement4.setLong(5, 0);
						}

						preparedStatement4.setString(6, coverageInsuredDO.getFuelTypeCd());
						preparedStatement4.setString(7, coverageInsuredDO.getVehicleMakeCd());
						// preparedStatement4.setDate(8, (Date)
						// coverageInsuredDO.getManufacturingDt());
						preparedStatement4.setString(8, coverageInsuredDO.getVehicleModelCd());
						// preparedStatement4.setDate(10, (Date)
						// coverageInsuredDO.getRegistrationDt());
						preparedStatement4.setString(9, coverageInsuredDO.getRegistrationNum());
						preparedStatement4.setLong(10, generatedPolicyKey);
						if (coverageInsuredDO.getBodyTypeCd() != null) {
							preparedStatement4.setString(11, coverageInsuredDO.getBodyTypeCd());
						} else {
							preparedStatement4.setString(11, null);
						}

						preparedStatement4.execute();
					}
				} else {
					PreparedStatement preparedStatement4 = null;
					String riderDetails = "INSERT INTO public.Riders(policy_no, Policy_Type, Plan_Type, sum_insured,proposed_policy,"
							+ "sdt, status, username, "
							// + "vehmanufactureyear,vehregistrationdate,
							+ "user_type,Product)" + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

					preparedStatement4 = c.prepareStatement(riderDetails);
					preparedStatement4.setInt(1, Integer.parseInt(policyDO.getPolicyNum()));
					preparedStatement4.setString(2, "MOTOR");
					preparedStatement4.setString(3, coverageDO.getPlanTypeCd());
					preparedStatement4.setString(4, coverageDO.getSumInsured().toString());
					preparedStatement4.setString(5, null);
					preparedStatement4.setString(6, null);
					preparedStatement4.setString(7, "ACTIVE");
					// preparedStatement4.setDate(8, (Date)
					// coverageInsuredDO.getManufacturingDt());
					preparedStatement4.setString(8, "Motor");
					// preparedStatement4.setDate(10, (Date)
					// coverageInsuredDO.getRegistrationDt());
					preparedStatement4.setString(9, "MotorAdmin");
					preparedStatement4.setString(10, coverageDO.getProductCd());

					preparedStatement4.execute();
				}
			}
			PreparedStatement preparedStatement5 = null;
			String inboxQuery = "INSERT INTO public.task_list(ref_no, task, task_date, task_type, id, user_type, username)VALUES (?, ?, ?, ?, ?, ?, ?);";
			preparedStatement5 = c.prepareStatement(inboxQuery);
			preparedStatement5.setString(1, policyDO.getPolicyNum());
			preparedStatement5.setString(2, createPolicyIO.getServiceDO().getService());

			java.util.Date d = new java.util.Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String date = dateFormat.format(d);

			preparedStatement5.setString(3, date);
			preparedStatement5.setString(4, "STP");
			preparedStatement5.setInt(5, Integer.parseInt(policyDO.getPolicyNum()));
			preparedStatement5.setString(6, "MotorAdmin");
			preparedStatement5.setString(7, "Motor");
			if (createPolicyIO.getServiceDO().getServiceTaskList().size() > 0) {

			} else {
				preparedStatement5.setString(4, null);
			}
			preparedStatement5.execute();

			c.commit();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void convertionDoc(CreatePolicyIO createPolicyIO) {
		Connection c = null;

		try {

			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mydb", "postgres", "root");
			c.setAutoCommit(false);

			if (createPolicyIO.getEntityDocumentDOs() != null && createPolicyIO.getEntityDocumentDOs().size() > 0) {
				for (EntityDocumentDO entityServiceDocumentDO : createPolicyIO.getEntityDocumentDOs()) {
					PreparedStatement preparedStatement = null;
					if ("MOTOR".equals(createPolicyIO.getPolicyDO().getPolicyTypeCd())) {
						String docQuery = "INSERT INTO public.case_req_tracking(\"Document_Category\",\"Policy_No\",\"Status\")VALUES (?, ? , ?);";
						preparedStatement = c.prepareStatement(docQuery);
						preparedStatement.setString(1, entityServiceDocumentDO.getDocumentTypeCd());
						preparedStatement.setString(2, createPolicyIO.getPolicyDO().getPolicyNum());
						preparedStatement.setString(3, "Received");
						preparedStatement.execute();
					}else if ("HEALTH".equals(createPolicyIO.getPolicyDO().getPolicyTypeCd())) {
						String docQuery = "INSERT INTO public.health_documents_details(doc_category ,doc,doc_status ,doc_gen_date ,doc_rec_date ,h_policy_no ) VALUES"
								+ " (?,?,?,?,?,?);";
						preparedStatement = c.prepareStatement(docQuery);
						preparedStatement.setString(1, entityServiceDocumentDO.getDocumentTypeCd());
						preparedStatement.setString(2, entityServiceDocumentDO.getDocumentTypeCd());
						preparedStatement.setString(3, "Received");
						preparedStatement.setDate(4, (Date) DateUtil.getInstance().systemDate());
						preparedStatement.setDate(5, (Date) DateUtil.getInstance().systemDate());
						preparedStatement.setString(6, createPolicyIO.getPolicyDO().getPolicyNum());
						
						preparedStatement.execute();
					}
				}
			}

				List<HashMapEntityDO> hashMapEntityDOs = hashMapEntityRepository
						.findByEntityGuid(createPolicyIO.getPolicyDO().getGuid());
				for (HashMapEntityDO hashMapEntityDO : hashMapEntityDOs) {
					PreparedStatement preparedStatement1 = null;
					String result = "INSERT INTO public.case_insured_rule(rule_name, rule_decision,remark,\"Policy_No\")"
							+ "VALUES (?, ?, ?, ?);";
					preparedStatement1 = c.prepareStatement(result);
					if (hashMapEntityDO.getHashMapDataList() != null
							&& hashMapEntityDO.getHashMapDataList().size() > 0) {
						for (HashMapDataDO hashMapDataDO : hashMapEntityDO.getHashMapDataList()) {
							if ("uw_result".equals(hashMapDataDO.getFieldKey())) {
								preparedStatement1.setString(2, hashMapDataDO.getFieldValue());
							} else if ("$ruleMessage".equals(hashMapDataDO.getFieldKey())) {
								preparedStatement1.setString(3, hashMapDataDO.getFieldValue());
							} else if ("rule_name".equals(hashMapDataDO.getFieldKey())) {
								preparedStatement1.setString(1, hashMapDataDO.getFieldValue());
							}
						}
						preparedStatement1.setString(4, createPolicyIO.getPolicyDO().getPolicyNum());
						preparedStatement1.execute();
					}
				}
			c.commit();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}
}