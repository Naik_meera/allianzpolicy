package com.metamorphosys.policy.utility;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtil {
	
	private static final Logger log = LoggerFactory.getLogger(CommonUtil.class);
	
	private static CommonUtil utility;

	public static CommonUtil getInstance()
	{
		if(null == utility)
		{
			utility = new CommonUtil();
		}
		
		return utility;
	}
	
	public static <T1 extends Object, T2 extends Object> void cloneObject(T1 entity, T2 entity2)
			throws IllegalAccessException, NoSuchFieldException {
		Class<? extends Object> copy1 = entity.getClass();
		Class<? extends Object> copy2 = entity2.getClass();

		 Field[] fromFields = copy1.getDeclaredFields();
		// Field[] toFields = copy2.getDeclaredFields();
		 Method[] transactionDOMethods = entity.getClass().getMethods();
		 for(Field fields :fromFields){
			for (Method method : transactionDOMethods){
	
				if (method.getName().startsWith("get")) {
					String attributeName = getFieldName(method);
					if(attributeName.equals(fields.getName())){
						Object value = null;
						try {
							value = method.invoke(entity, null);
		
						} catch (IllegalAccessException e) {
							log.error("IllegalAccessException: " + e);
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							log.error("IllegalArgumentException: " + e);
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							log.error("InvocationTargetException: " + e);
							e.printStackTrace();
						}
		
						if (value instanceof List) // handle list
						{
							
						} else {
							
							//String setAttribute = String.format("set%C%s",attributeName.charAt(0), attributeName.substring(1));
								Field field = copy2.getDeclaredField(attributeName);
								field.setAccessible(true);
								field.set(entity2, value);
							}
						}
					}
				break;
				}
			}
	}

	public static String getFieldName(Method method)
	{
	    try
	    {
	        Class<?> clazz=method.getDeclaringClass();
	        BeanInfo info = Introspector.getBeanInfo(clazz);  
	        PropertyDescriptor[] props = info.getPropertyDescriptors();  
	        for (PropertyDescriptor pd : props) 
	        {  
	            if(method.equals(pd.getWriteMethod()) || method.equals(pd.getReadMethod()))
	            {
	                return pd.getName();
	            }
	        }
	    }
	    catch (IntrospectionException e) 
	    {
	        e.printStackTrace();
	    }
	    catch (Exception e) 
	    {
	        e.printStackTrace();
	    }


	    return null;
	}
}
