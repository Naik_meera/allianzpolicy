package com.metamorphosys.policy.utility;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DateAdapter implements JsonDeserializer<Date>, JsonSerializer<Date> {

	private static final Logger log = LoggerFactory.getLogger(DateAdapter.class);
//	private DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//	private DateFormat timeFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	public JsonElement serialize(Date date, Type type, JsonSerializationContext context) 
	{	
		JsonPrimitive returnValue = null;
		
		try
		{
			if(null != date)
			{
				//date = new JsonPrimitive(timeFormatter.format(date));
				returnValue = new JsonPrimitive(date.getTime());
			}
			else
			{
				log.error("date is null!!!");
			}
		}
		catch(Exception e)
		{
			log.error("Error ["+date+"]: "+e.getMessage());
		}
		
		return returnValue;
	}	
	public Date deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException 
	{	
		//log.info("Date is :"+json.getAsString());
		Date date = null;
		String newStr=null;
		Long dateInJson = Long.valueOf(json.getAsString());
		try
		{
			
			if(!"".equals(json.toString()))
			{
					if( "class java.util.Date".equals(type.toString())){
						date=new java.util.Date(dateInJson);
					}else if( "class java.sql.Date".equals(type.toString())){
						date = new java.sql.Date(dateInJson);
					}else if( "class java.sql.Timestamp".equals(type.toString())){
						date = new Timestamp(dateInJson);
					}else{
						date = new Timestamp(dateInJson);
					}
			}
			//log.info("Deserialized Date:"+date.toString());
		}catch(Exception e)
		{
			log.error("Cannot convert ["+type.getTypeName()+"] to number: "+json.toString());
		}
        
        return date;
	}
}