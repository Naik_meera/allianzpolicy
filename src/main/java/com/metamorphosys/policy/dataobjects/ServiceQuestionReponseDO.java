package com.metamorphosys.policy.dataobjects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="SERVICEQUESTIONRESPONSE")
public class ServiceQuestionReponseDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="serviceQuestionResponseId_seq") 
	@SequenceGenerator(name="serviceQuestionResponseId_seq",sequenceName="SERVICEQUESTIONRESPONSEID_SEQ",allocationSize=1) Long id;

	public Long getId() {
		return id;
	}
}
