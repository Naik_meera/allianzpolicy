package com.metamorphosys.policy.dataobjects.common;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AdditionalFieldDO extends ChildBaseDO {

	private String fieldKey;
	
	private String fieldValue;

	public String getFieldKey() {
		return fieldKey;
	}

	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

}
