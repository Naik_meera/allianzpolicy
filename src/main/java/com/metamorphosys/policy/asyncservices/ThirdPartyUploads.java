package com.metamorphosys.policy.asyncservices;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.dataobjects.master.ApplicationDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntitySignatureDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.RetryServiceDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.transaction.RetryServiceRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;
import com.metamorphosys.policy.utility.SerializationUtility;

@Service
public class ThirdPartyUploads {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdPartyUploads.class);
	
	@Autowired
	ApplicationRepository applicationRepository;
	
	@Autowired
	RetryServiceRepository retryServiceRepository; 
	
	@Async("threadPoolTaskExecutor")
	public void thirdPartyUpload(CreatePolicyIO io) {
		try{
			
			if(io.getEntityDocumentDOs()!=null && io.getEntityDocumentDOs().size()>0){
				for(EntityDocumentDO entityDocumentDO: io.getEntityDocumentDOs()){
					// first check whether entityImage null or not. If not null then only proceed 
					if(entityDocumentDO.getEntityImage() != null){
						convertToDocImage(entityDocumentDO, io.getPolicyDO());					
					}
					entityDocumentDO.setEntityGuid(io.getPolicyDO().getGuid());
				}
			}

			if(io.getEntitySignatureDOs()!=null && io.getEntitySignatureDOs().size()>0){
				for(EntitySignatureDO entitySignatureDO: io.getEntitySignatureDOs()){
					if(entitySignatureDO.getSignature()!=null)
					{
						convertToSignImage(entitySignatureDO, io.getPolicyDO());
					}
					entitySignatureDO.setEntityGuid(io.getPolicyDO().getGuid());
				}
			}
			
			//third Party Service to  upload images
			// Calling third party service
			boolean isvalid=true;
			boolean entityDocumentUploadStatus = true;
			boolean signatureUploadStatus = true;
			String authToken = "";
			String systemUserId = "";
			String systemUserPwd = "";
			systemUserId = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERID).getValue();
			systemUserPwd = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERPWD).getValue();
			//Code added by Anna to make third party calls on 30/01/2017

			HttpHeaders httpHeader = new HttpHeaders();
			httpHeader.set("channel", "mobile");
			HttpEntity entityauth = new HttpEntity(httpHeader);

			String internalAuthURL = applicationRepository.findByKey(InsureConnectConstants.Path.AUTHINTERFACEPATH).getValue()
					+ "/" + systemUserId + "/" + systemUserPwd;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> reponseObject = restTemplate.exchange(internalAuthURL, HttpMethod.GET, entityauth, String.class);

			if(HttpStatus.OK.equals(reponseObject.getStatusCode())) {
				HashMap<String, String> excternalAuthAPIresponse = (HashMap<String, String>) SerializationUtility.getInstance()
						.fromJson(reponseObject.getBody(), HashMap.class);
				authToken = excternalAuthAPIresponse.get("authToken");
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);
				httpHeaders.set("Authorization", authToken);
				httpHeaders.set("referenceId", io.getPolicyDO().getProposalNum());
				httpHeaders.set("agentid", io.getPolicyDO().getAgentId());
				httpHeaders.set("source", "MOBILE");
				httpHeaders.set("channel", "mobile");

				String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYINTERFACEPATH).getValue();
				for(EntityDocumentDO entityDocumentDO:io.getEntityDocumentDOs())
				{
					// first check whether entityImage null or not. If not null then only proceed 
					if(entityDocumentDO.getEntityImage() != null){
						httpHeaders.set("documentType", "EntityDocumentDO");
						HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(entityDocumentDO), httpHeaders);

						// individual image upload call starts. 
						ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
						if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
							entityDocumentUploadStatus = false;
							entityDocumentDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
							break;
						}else{
							HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
							//EntityDocumentDO tempDoc = (EntityDocumentDO) SerializationUtility.getInstance().fromJson(hashMap.get("entityDocument"),
							//EntityDocumentDO.class);
							entityDocumentDO.setReferenceID(hashMap.get("imageReferenceId"));
							entityDocumentDO.setEntityImageName(hashMap.get("imageFileName"));
							entityDocumentDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
						}
					}	
				}
				for(EntitySignatureDO entitySignatureDO:io.getEntitySignatureDOs())
				{
					if(entitySignatureDO.getSignature() != null){
						//					HttpHeaders httpHeaders = new HttpHeaders();
						//					httpHeaders.setContentType(MediaType.APPLICATION_JSON);
						httpHeaders.set("documentType", "EntitySignatureDO");
						HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(entitySignatureDO), httpHeaders);
						ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
						if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
							signatureUploadStatus = false;
							entitySignatureDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
							break;
						}else{
							HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
							entitySignatureDO.setReferenceID(hashMap.get("imageReferenceId"));
							entitySignatureDO.setEntityImageName(hashMap.get("imageFileName"));
							entitySignatureDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
						}
					}
				}
			}else{
				isvalid = false;
				RetryServiceDO retryServiceDO = new RetryServiceDO();
				retryServiceDO.setServiceId(io.getPolicyDO().getPolicyNum());
				retryServiceDO.setServiceName("BINDING");
				retryServiceDO.setStatusCd(InsureConnectConstants.Status.PENDING);
				retryServiceDO.setNoOfRetries(1);
				retryServiceRepository.save(retryServiceDO);
			}

			//Based on the status of Uploded document and signature , need to  


			if(isvalid && entityDocumentUploadStatus && signatureUploadStatus){
				String status=submitBinding(io, authToken);
				LOGGER.info("RESULT OF SUBMIT BINDING "+status);
				if(status == null || !status.equals("PASSED")){
					saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
				}
			}else{
				saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
			}

		}catch(Exception e){
			e.printStackTrace();
			saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
		}
	}
	
	@Async("threadPoolTaskExecutor")
	public void aliThirdPartyUpload(CreatePolicyIO io) {
		try{
			if(io.getEntityDocumentDOs()!=null && io.getEntityDocumentDOs().size()>0){
				for(EntityDocumentDO entityDocumentDO: io.getEntityDocumentDOs()){
					// first check whether entityImage null or not. If not null then only proceed 
					if(entityDocumentDO.getEntityImage() != null){
						convertToDocImage(entityDocumentDO,  io.getPolicyDO());					
					}
					entityDocumentDO.setEntityGuid(io.getPolicyDO().getGuid());
				}
			}

			if(io.getEntitySignatureDOs()!=null && io.getEntitySignatureDOs().size()>0){
				for(EntitySignatureDO entitySignatureDO: io.getEntitySignatureDOs()){
					if(entitySignatureDO.getSignature()!=null)
					{
						convertToSignImage(entitySignatureDO, io.getPolicyDO());
					}
					entitySignatureDO.setEntityGuid(io.getPolicyDO().getGuid());
				}
			}
			//third Party Service to  upload images
			// Calling third party service
			boolean isvalid=true;
			boolean entityDocumentUploadStatus = true;
			boolean signatureUploadStatus = true;
			String authToken = "";
			String systemUserId = "";
			String systemUserPwd = "";
			systemUserId = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERID).getValue();
			systemUserPwd = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERPWD).getValue();
			//Code added by Anna to make third party calls on 30/01/2017

			HttpHeaders httpHeader = new HttpHeaders();
			httpHeader.set("channel", "mobile");
			HttpEntity entityauth = new HttpEntity(httpHeader);

			String internalAuthURL = applicationRepository.findByKey(InsureConnectConstants.Path.AUTHINTERFACEPATH).getValue()
					+ "/" + systemUserId + "/" + systemUserPwd;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> reponseObject = restTemplate.exchange(internalAuthURL, HttpMethod.GET, entityauth, String.class);

			if(HttpStatus.OK.equals(reponseObject.getStatusCode())) {
				HashMap<String, String> excternalAuthAPIresponse = (HashMap<String, String>) SerializationUtility.getInstance()
						.fromJson(reponseObject.getBody(), HashMap.class);
				authToken = excternalAuthAPIresponse.get("authToken");
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);
				httpHeaders.set("Authorization", authToken);
				httpHeaders.set("referenceId", io.getPolicyDO().getProposalNum());
				httpHeaders.set("agentid", io.getPolicyDO().getAgentId());
				httpHeaders.set("source", "MOBILE");
				httpHeaders.set("channel", "mobile");

				String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYINTERFACEPATH).getValue();
				for(EntityDocumentDO entityDocumentDO:io.getEntityDocumentDOs())
				{
					// first check whether entityImage null or not. If not null then only proceed 
					if(entityDocumentDO.getEntityImage() != null){
						httpHeaders.set("documentType", "EntityDocumentDO");
						HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(entityDocumentDO), httpHeaders);
						// individual image upload call starts. 
						ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
						if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
							entityDocumentUploadStatus = false;
							entityDocumentDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
							break;
						}else{
							HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
							//EntityDocumentDO tempDoc = (EntityDocumentDO) SerializationUtility.getInstance().fromJson(hashMap.get("entityDocument"),
							//EntityDocumentDO.class);
							entityDocumentDO.setReferenceID(hashMap.get("imageReferenceId"));
							entityDocumentDO.setEntityImageName(hashMap.get("imageFileName"));
							entityDocumentDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
						}
					}	
				}
				for(EntitySignatureDO entitySignatureDO:io.getEntitySignatureDOs())
				{
					if(entitySignatureDO.getSignature() != null){
						//					HttpHeaders httpHeaders = new HttpHeaders();
						//					httpHeaders.setContentType(MediaType.APPLICATION_JSON);
						httpHeaders.set("documentType", "EntitySignatureDO");
						HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(entitySignatureDO), httpHeaders);
						ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
						if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
							signatureUploadStatus = false;
							entitySignatureDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
							break;
						}else{
							HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
							entitySignatureDO.setReferenceID(hashMap.get("imageReferenceId"));
							entitySignatureDO.setEntityImageName(hashMap.get("imageFileName"));
							entitySignatureDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
						}
					}
				}
			}else{
				isvalid = false;
				RetryServiceDO retryServiceDO = new RetryServiceDO();
				retryServiceDO.setServiceId(io.getPolicyDO().getPolicyNum());
				retryServiceDO.setServiceName("BINDING");
				retryServiceDO.setStatusCd(InsureConnectConstants.Status.PENDING);
				retryServiceDO.setNoOfRetries(1);
				retryServiceRepository.save(retryServiceDO);
			}

			//Based on the status of Uploded document and signature , need to  


			if(isvalid && entityDocumentUploadStatus && signatureUploadStatus){
				String status=aliSubmitBinding(io, authToken);
				LOGGER.info("RESULT OF SUBMIT BINDING "+status);
				if(status == null || !status.equals("PASSED")){
					saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
				}
			}else{
				saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
			}

		}catch(Exception e){
			e.printStackTrace();
			saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
		}
	}
	
	private void saveTaskForRetry(String serviceId, String serviceName, String serviceStatus){
		LOGGER.info("Saving retry service.............");
		RetryServiceDO retryServiceDO = new RetryServiceDO();
		retryServiceDO.setServiceId(serviceId);
		retryServiceDO.setServiceName(serviceName);
		retryServiceDO.setStatusCd(serviceStatus);
		retryServiceDO.setNoOfRetries(1);
		retryServiceRepository.save(retryServiceDO);

	}

	private String submitBinding(CreatePolicyIO io, String authToken) {
		try{

			//third Party Service for submit binding
			//Anna Make Change on 31/1/17 
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			httpHeaders.set("Authorization", authToken);
			HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(io.getPolicyDO()), httpHeaders);
			String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.POLICYBINDINGDPATH).getValue();
			//log.
			ResponseEntity<String> submitBindingResponse = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
			HashMap<String, String> resultHashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(submitBindingResponse.getBody(), HashMap.class);
			return resultHashMap.get("submitBindingStatus");

		}catch(Exception e){
			e.printStackTrace();
		}
		return null;

	}
	private String aliSubmitBinding(CreatePolicyIO io, String authToken) {
		try{

			//third Party Service for submit binding
			//Anna Make Change on 31/1/17 
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			httpHeaders.set("Authorization", authToken);
			HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(io.getPolicyDO()), httpHeaders);
			String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.ALIPOLICYBINDINGDPATH).getValue();
			//log.
			ResponseEntity<String> submitBindingResponse = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
			HashMap<String, String> resultHashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(submitBindingResponse.getBody(), HashMap.class);
			return resultHashMap.get("submitBindingStatus");

		}catch(Exception e){
			e.printStackTrace();
		}
		return null;

	}
	
	public void convertToDocImage(EntityDocumentDO entityDocumentDO, PolicyDO policyDO) {
		try {
			// Converting a Base64 string to image byte array
			String docImage = entityDocumentDO.getEntityImage().substring(entityDocumentDO.getEntityImage().indexOf(",")+1);
			String imageType = getImageType(entityDocumentDO.getEntityImage());
			byte[] imageByteArray = decodeImage(docImage);
			entityDocumentDO.setEntityImageMimeType(imageType);
			String fileName="";
			if(entityDocumentDO.getDocumentSubTypeCd().trim().equalsIgnoreCase("IDENTITY")) {
				fileName=  entityDocumentDO.getDocumentTypeCd()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+"_"+entityDocumentDO.getId()+"."+imageType;
			}else if(entityDocumentDO.getDocumentTypeCd().trim().equalsIgnoreCase("ACCESSORYIMAGE")) {
				fileName=  "VEHICLE_ACCESSORY_"+entityDocumentDO.getReferenceTypeCd()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+"_"+entityDocumentDO.getId()+"."+imageType;
			}	else {
				fileName= entityDocumentDO.getDocumentSubTypeCd()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+"_"+entityDocumentDO.getId()+"."+imageType;
			}
			
			entityDocumentDO.setEntityImageName(fileName);
			ApplicationDO appDO=applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYDOCPATH);
			String path = appDO.getValue() +"/"+ policyDO.getProposalRefNum()+"/"+fileName;
			entityDocumentDO.setImageURL(path);
			LOGGER.info("File Path"+path);
			imageCreation(path, imageByteArray, fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void convertToSignImage(EntitySignatureDO entitySignatureDO, PolicyDO policyDO) {
		try {

			// Converting a Base64 string to image byte array
			String signature = entitySignatureDO.getSignature().substring(entitySignatureDO.getSignature().indexOf(",")+1);
			String imageType = getImageType(entitySignatureDO.getSignature());
			entitySignatureDO.setEntityImageMimeType(imageType);
			byte[] imageByteArray = decodeImage(signature);

			String fileName=  entitySignatureDO.getEntitySignatureID()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+entitySignatureDO.getEntityID()+"."+imageType;
			entitySignatureDO.setEntityImageName(fileName);
			ApplicationDO appDO=applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYDOCPATH);
			String path = appDO.getValue() +"/"+ policyDO.getProposalRefNum()+"/"+entitySignatureDO.getReferenceTypeCd()+"/"+fileName;
			LOGGER.info("Path"+path);
			entitySignatureDO.setImageURL(path);
			imageCreation(path, imageByteArray, fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void imageCreation(String path,byte[] imageByteArray, String fileName){
		try{
			File directory = new File(String.valueOf(path));
			if (! directory.getParentFile().exists()){
				directory.getParentFile().mkdir();
			}		
			// Write a image byte array into file system
			FileOutputStream fileOutputStream = new FileOutputStream(directory);
			fileOutputStream.write(imageByteArray);
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static byte[] decodeImage(String imageString) {
		return Base64.decodeBase64(imageString);
	}

	public String getImageType(String signature){
		String imageType = "jpg";
		imageType = signature.substring(0, signature.indexOf(";"));
		imageType = imageType.substring(imageType.indexOf("/")+1);
		return imageType;
	}
	
	public String convertTODate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String format = formatter.format(date);
		return format;
	}
	
	public String convertTOString(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(date);
		return format;
	}
}
